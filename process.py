# -*- coding: utf-8 -*-
"""
DRUGS/Corona-AI Project module: process.py

Created on Mon Jan 15 11:52:26 2018

This module takes settings.ini file and processes the data according to this
single set of parameters. Processing includes data import, interactome construction,
profile propagations for both disease and drugs, their cross-correlation and
candidate drug ranking. Machine learning based classification accuracy estimation
is also optionally performed.

It should be executed by running: 

python process.py settings.ini

It will produce intermediate files in ../SCRATCH and log in ../RESULTS by default.

Anaconda Python 3.x 64bit is recommended with Pandas, Scikit-learn and NumPy pre-installed.

--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Co-Developer: Ms. Guadalupe G Gonzalez Pigorini (mailto:g.gonzalez-pigorini17@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2020 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

#Loading global modules and dependencies
import pandas as pd;
import numpy as np;
import os;
import sys;


import time;

from sklearn.linear_model import LogisticRegression;
from sklearn.model_selection import StratifiedKFold;
from sklearn.metrics import balanced_accuracy_score;

from scipy.stats.mstats import spearmanr


def sensitivity(y, ypred):
    """
    Calculate sensitivity (i.e. percent of correctly predicted positive class samples)

    Parameters
    ----------
    y : 1D numpy array of int type
        True class labels, 1 for positive class, 0 or anything else for negative.
    ypred : 1D numpy array of int type
        Predicted class labels, 1 for positive class, 0 or anything else for negative.

    Returns
    -------
    float
        Fraction of the correctly predicted positive class samples.

    """
    mask = y == 1;
    return np.sum(y[mask] == ypred[mask]).astype(np.float64) / np.sum(mask);


def specificity(y, ypred):
    """
    Calculate specificity (i.e. percent of correctly predicted negative class samples)

    Parameters
    ----------
    y : 1D numpy array of int type
        True class labels, 1 or anything else for positive class, 0 for negative.
    ypred : 1D numpy array of int type
        Predicted class labels, 1 or anything else for positive class, 0 for negative.

    Returns
    -------
    float
        Fraction of the correctly predicted negative class samples.

    """
    mask = y == 0;
    return np.sum(y[mask] == ypred[mask]).astype(np.float64) / np.sum(mask);


def get_accuracies(X, y, train_index, test_index):
    """
    Fit the model and calculate a set of statistical metrics for the input data X and
    class labels y according to the train/test split indices.

    Parameters
    ----------
    X : 2D float numpy array (n_samples, n_features)
        Input data matrix.
    y : 1D int numpy array (n_samples, )
        True class labels, 1 for positive class, 0 for negative class.
    train_index : 1D int numpy array (n_train_samples, ) or boolean mask array
        Indices of the train set samples.
    test_index : 1D int numpy array (n_test_samples, ) or boolean mask array
        Indices of the test set samples.

    Returns
    -------
    int
        Number of positive class samples in training set.
    int
        Number of positive class samples in test set.
    int
        Total number of samples in training set.
    int
        Total number of samples in test set.
    float
        balanced accuracy for the training set.
    float
        balanced accuracy for the test set.
    float
        sensitivity for the training set.
    float
        sensitivity for the test set.
    float
        specificity for the training set.
    float
        specificity for the test set.

    """
    #Make sure X is of correct shape for the case of a single sample
    if len(X.shape) == 1:
        X = X.reshape(-1, 1);

    #Split data into train and test sets
    X_train, X_test = X[train_index], X[test_index];
    y_train, y_test = y[train_index], y[test_index];
    
    #Get rid of samples with NaN values
    dmask = ~np.any(np.isnan(X_train), axis = 1);
    
    X_train = X_train[dmask, :];
    y_train = y_train[dmask];

    dmask = ~np.any(np.isnan(X_test), axis = 1);
    
    X_test = X_test[dmask, :];
    y_test = y_test[dmask];
            
    #Fit classifier and predict y labels
    clf = LogisticRegression(class_weight = 'balanced', fit_intercept = True, solver = 'liblinear').fit(X_train, y_train);
    y_pred_train = clf.predict(X_train);
    y_pred_test = clf.predict(X_test);
    
    #Return stats
    return np.sum(y_train == 1),\
           np.sum(y_test == 1),\
           len(y_train),\
           len(y_test),\
           balanced_accuracy_score(y_train, y_pred_train),\
           balanced_accuracy_score(y_test, y_pred_test),\
           sensitivity(y_train, y_pred_train),\
           sensitivity(y_test, y_pred_test),\
           specificity(y_train, y_pred_train),\
           specificity(y_test, y_pred_test);
            


#Checking if the system is right and if the module is run as a script instead of being imported
if __name__== '__main__':
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
else:
    print('This module is not meant to be imported, but should be run as a script instead!')
    quit();
    
#Figuring out and adding the path to local modules    
module_path = os.path.dirname(os.path.realpath(__file__));
sys.path.append(module_path);

#Loading local modules
from proc.utils.printlog import start_log, stop_log, printlog;  #print and log printed messages in one go

#different functions used by the algorithm
from proc.utils.utils import load_global_settings, memory_report, cleanup_string;


#Timing functions for standard stats output
from proc.utils.timing import tic, toc;

#Command line options
from proc.procconfig import Default_options as cmdline_options;

#Manager for command line options
from proc.utils.cmdline import OptionsHolder;

#Random Walk method
from proc.randomwalk import randomwalk_sparse_matrix;

#Pearson correlation
from proc.correlate import correlate_all;

def export_vector_rnk(vector, gene_names, fname):
    """
    Writes gene scores for GSEA analysis to a file

    Parameters
    ----------
    vector : 1D numpy array of floats
        Vector of gene perturbations/gene scores.
    gene_names : 1D list of str
        Corresponding gene names list.
    fname : str
        File name for the output file.

    Returns
    -------
    None.

    """
    with open(fname, 'w') as fout:
        fout.write('LABEL\tWEIGHT\n');
        for i in range(len(vector)):
            for j in gene_names[i]:
                fout.write('%s\t%s\n'%(j, vector[i]));
        

def perform_propagation(genes_sparse, dataset, dataset_label, genes, source_indices, target_indices, c):
    """
    Initialize and propagate profiles using RandomWalk algorithm. Automatically estimate c constant if needed.

    Parameters
    ----------
    genes_sparse : list of str
        List of gene connections for N samples in the form of pipe delimited string, e.g. [ '100|120|2400', '44|585', ...].
    dataset : DataFrame
        Pandas DataFrame containing sample information (disease or compounds).
    dataset_label : str
        Dataset type as a string, e.g. 'disease', 'positive_class', 'negative_class' etc.
    genes : DataFrame
        Pandas DataFrame of genes information in interactome. (n_genes == n_features)
    source_indices : numpy array of int, 1D (n_gene_connections, )
        Indices of the genes from which the connections start. 
    target_indices : numpy array of int, 1D (n_gene_connections, )
        Indices of the genes on which the connections end.
    c : float
        c constant for the RandomWalk restart probability. Should be between 0.0 and 1.0. If negative value is submitted,
        c will be evaluated automatically by first propagating with c == 0.0 and then using c == 10 * max(propagated_genes) with individual c value
        for each sample.

    Returns
    -------
    vector : 2D numpy array of float
        Propagated profiles (n_samples, n_genes).

    """
    #Saving intermediate profiles
    printlog('Saving intermediate %s profile dataset.'%dataset_label);
    dataset.to_csv(os.path.join(scratch_path, '%s.csv'%dataset_label), encoding = 'utf-8');
    
    vector = np.zeros((len(genes_sparse), len(genes.index)), dtype = np.float64);
    
    printlog('Preparing propagation.');
    for i in range(len(genes_sparse)):
        gene_ids = genes_sparse[i].split('|');
        for j in range(len(gene_ids)):
            value = gene_ids[j];
            if ':' in value:
                index, weight = value.split(':')[:2];
                vector[i, int(index)] = float(weight);
            else:
                vector[i, int(value)] = 1.0;
        
        
    vector.astype(np.float32).tofile(os.path.join(scratch_path, '%s_starting_vectors.dat'%dataset_label));
    
    if c >= 0.0:
        c_values = c;
    else:
        printlog('Getting adaptive c...');
        original_vector = vector;
    
        vector, ncyc, epses = randomwalk_sparse_matrix(vector, 
                                          source_indices, 
                                          target_indices, 
                                          weights = None, 
                                          c = 0.0, 
                                          max_cycles = 300, 
                                          normalize_input = True, 
                                          normalize_output = True, 
                                          normalize_connections = True, 
                                          eps = 1.0e-6);
        
        if len(vector.shape) > 1 and vector.shape[0] > 1:
            c_values = np.max(vector, axis = 1) * 10.0;
        else:
            c_values = np.max(vector) * 10.0;
        
        c_values.tofile(os.path.join(scratch_path, '%s_adaptive_c_values.dat'%dataset_label));
        dataset['PageRank_Adaptive_c'] = c_values.tolist();
        dataset.to_csv(os.path.join(scratch_path, '%s.csv'%dataset_label), encoding = 'utf-8');
        vector = original_vector;
        
    
    printlog('Propagating profiles....')
    vector, ncyc, epses = randomwalk_sparse_matrix(vector, 
                                      source_indices, 
                                      target_indices, 
                                      weights = None, 
                                      c = c_values, 
                                      max_cycles = 300, 
                                      normalize_input = True, 
                                      normalize_output = True, 
                                      normalize_connections = True, 
                                      eps = 1.0e-6);
    
    printlog('Finished at cycle %s with max eps of %s, mean eps of %s and median eps of %s'%(ncyc, np.max(epses), np.mean(epses), np.median(epses)));
    
    printlog('Saving propagated vectors for %s profile.'%dataset_label)
    vector.astype(np.float32).tofile(os.path.join(scratch_path, '%s_propagated_vectors.dat'%dataset_label));

    return vector



def correlate_all_spearman(vector1, vector2):
    """
    Calculate Spearman correlation coefficient(s) for two vectors or 2D arrays.
    2D arrays are to be in the format (n_samples, n_features).
    1D vector will be automatically treated as (1, n_features) array.
    This function will calculate Spearman correlation coefficients for each combination
    of the input samples from vectors 1&2 and return a matrix (n_samples, m_samples) where
    n_samples corresponds to the number of samples in vector1 and m_samples corresponds to
    the number of samples in vector2. Number of features must be equal between vector1 and vector2.

    Parameters
    ----------
    vector1 : numpy array (1D or 2D)
        Vector (1D) or 2D array of floats.
    vector2 : numpy array (1D or 2D)
        Vector (1D) or 2D array of floats.

    Returns
    -------
    result : 2D numpy array
        (n_samples, m_samples) matrix of Spearman correlation coefficients.
    pvalues : 2D numpy array
        (n_samples, m_samples) matrix of pvalues provided by the spearmanr method.

    """
    result = np.zeros((len(vector1), len(vector2)), dtype = np.float64);
    pvalues = np.zeros((len(vector1), len(vector2)), dtype = np.float64);
    if len(vector1.shape) == 1:
        vector1 = vector1.reshape(1, -1);
    if len(vector2.shape) == 1:
        vector2 = vector2.reshape(1, -1);
        
    for i in range(vector1.shape[0]):
        for j in range(vector2.shape[0]):
            corr, pval = spearmanr(vector1[i, :], vector2[j, :])
            result[i, j] = corr;
            pvalues[i, j] = pval;
        
    
    return result, pvalues


if __name__ == "__main__": 
    
    
    ################################### Initialization section #########################################

    tic(); #Start default timer
    
    #Create command line options handler
    settings = OptionsHolder(__doc__, cmdline_options); 
    
    settings.description = 'DRUGS/Corona-AI module';
    
    printlog(settings.program_description)   
    
    #Parse command line parameters
    try:
        settings.parse_command_line_args()   
    except Exception as inst:
        printlog('!!! Error in command line parameters: !!!');
        printlog(inst);
        printlog('\nRun python ' + sys.argv[0] + ' --help for command line options information!');
        sys.exit(-1);

    settings.parse_command_line_args();

    #Initialize log file if logfile parameter is set. Overwrite existing logfile
    #if it already exists and overwrite_logfile is set to yes. 
    parameters = settings.parameters;
    if parameters['logfile'] != '':
        start_log(parameters['logfile'], overwrite_existing = (parameters['overwrite_logfile'] == 'yes'), verbosity_level = parameters['verbose']);
        #Repeat prining of the module description in order to write it to the initialized log file as well.
        #Second printing of the description to the screen is suppressed via print_enabled = False, 
        #only printing to log is enabled.
        printlog(settings.program_description, print_enabled = False);

    #Print when the script has started    
    printlog('Started on %s ...'%(time.strftime("%a, %d %b %Y at %H:%M:%S")));      
    
    #Print values of parameters from command line parsing to be used by the script    
    printlog(settings.format_parameters());
    
    #Load settings.ini
    settings_file = os.path.realpath(os.path.expanduser(parameters['Settings_INI_file']));
    if not os.path.isfile(settings_file):
        raise IOError('Settings file %s not found or is not a file! Please check your input!'%settings_file);

    settings_values = load_global_settings(settings_file);
    
    #Use settings file name as a subfolder for results
    output_subfolder = os.path.splitext(os.path.basename(settings_file))[0];

    #Loading compounds
    compounds_file = settings_values['Default Paths']['Compounds'];
    if not os.path.isfile(compounds_file):
        raise IOError('Compounds file %s not found or is not a file! Please check your input!'%compounds_file);

    compounds = pd.read_csv(compounds_file, index_col = 0, encoding = 'utf-8');
    printlog('%s compounds loaded.'%len(compounds.index));
    
    compound_genes_column = cleanup_string(settings_values['Default Paths']['Compound_Genes']);
    printlog('"%s" is used as the compound genes column.'%compound_genes_column);
    
    label_column = cleanup_string(settings_values['Default Paths']['Label_column']);
    printlog('"%s" is used as the class label column.'%label_column);
    
    #Loading disease profiles
    disease_file = settings_values['Default Paths']['Disease'];
    if not os.path.isfile(disease_file):
        raise IOError('Disease file %s not found or is not a file! Please check your input!'%disease_file);
        
    disease = pd.read_csv(disease_file, index_col = 0, encoding = 'utf-8');
    printlog('%s gene profiles loaded for the disease.'%len(disease.index));
        
    disease_genes_column = cleanup_string(settings_values['Default Paths']['Disease_Genes']);
    printlog('"%s" is used as the disease genes column'%disease_genes_column);
    
    #Loading interactome
    interactome_path = settings_values['Default Paths']['Interactome_Path'];
    if not os.path.isdir(interactome_path):
        raise IOError('Interactome path %s not found!'%interactome_path);
        
    genes = pd.read_csv(os.path.join(interactome_path, 'genes.csv.gz'), index_col = 0, encoding = 'utf-8');
    printlog('%s genes loaded for interactome.'%len(genes.index));
    
    gene_connections = pd.read_csv(os.path.join(interactome_path, 'gene_connections.csv.gz'),  encoding = 'utf-8');
    printlog('%s gene connections loaded for interactome'%len(gene_connections.index));
    
    #Setting output paths
    results_path = os.path.join(settings_values['Default Paths']['Results_Path'], output_subfolder);
    if not os.path.exists(results_path):
        os.makedirs(results_path);
        printlog('%s path created.'%results_path);
    
    scratch_path = os.path.join(settings_values['Default Paths']['Scratch_Path'], output_subfolder);
    if not os.path.exists(scratch_path):
        os.makedirs(scratch_path);
        printlog('%s path created.'%scratch_path);


    compound_connections = float(settings_values['Network Propagation Settings']['compound_connections']);

    #Network propagation settings
    
    gene_compound_threshold = float(settings_values['Network Propagation Settings']['gene_compound_threshold']);
    printlog('gene_compound_threshold: %s'%gene_compound_threshold);
    
    top_genes = int(settings_values['Network Propagation Settings']['top_genes']);
    printlog('top_genes: %s'%top_genes);
    
    STRING_threshold = float(settings_values['Network Propagation Settings']['STRING_threshold']);
    printlog('STRING_threshold: %s'%STRING_threshold);
    
    BioPlex_threshold = float(settings_values['Network Propagation Settings']['BioPlex_threshold']);
    printlog('BioPlex_threshold: %s'%BioPlex_threshold);
    
    compound_PageRank_c = float(settings_values['Network Propagation Settings']['Compound_PageRank_c']);
    printlog('Compound_PageRank_c: %s'%compound_PageRank_c);
    
    disease_PageRank_c = float(settings_values['Network Propagation Settings']['Disease_PageRank_c']);
    printlog('Disease_PageRank_c: %s'%disease_PageRank_c);
    
    #Switches whether to propagate profiles or try to read existing ones from Scratch subfolder from previous runs
    disease_propagation = settings_values['Network Propagation Settings']['disease_propagation'].lower();
    compounds_propagation = settings_values['Network Propagation Settings']['compounds_propagation'].lower();

    #Load analysis settings
    do_analysis = settings_values['Analysis Setting']['do_analysis'].lower() == 'yes';
    printlog('Do analysis: %s'%do_analysis);
    
    do_prediction = settings_values['Analysis Setting']['do_prediction'].lower() == 'yes';
    printlog('Do predictions: %s'%do_prediction);
    
    export_multiplied = settings_values['Analysis Setting']['export_multiplied'].lower() == 'yes';
    printlog('Export RNK files for multiplied profiles: %s'%export_multiplied);

    export_original = settings_values['Analysis Setting']['export_original'].lower() == 'yes';
    printlog('Export RNK files for original_profiles: %s'%export_original);

    export_disease = settings_values['Analysis Setting']['export_disease'].lower() == 'yes';
    printlog('Include RNK files for disease: %s'%export_disease);

    export_positive = settings_values['Analysis Setting']['export_positive'].lower() == 'yes';
    printlog('Include RNK files for positive class: %s'%export_positive);

    export_negative = settings_values['Analysis Setting']['export_negative'].lower() == 'yes';
    printlog('Include RNK files for negative class: %s'%export_negative);

    export_other = settings_values['Analysis Setting']['export_other'].lower() == 'yes';
    printlog('Include RNK files for other class: %s'%export_other);
    
    #Machine learning section
    n_repeats = int(settings_values['Machine Learning Settings']['N_repeats']);
    cv_folds = int(settings_values['Machine Learning Settings']['CV_folds']);
    if do_analysis:
        printlog('N cycles: %s \nN-folds: %s'%(n_repeats, cv_folds));

    #If exists it will override automatic split generation
    external_split_file = cleanup_string(settings_values['Machine Learning Settings']['external_split_file']);
    if external_split_file != '':
        if not os.path.isabs(external_split_file):
            external_split_file = os.path.realpath(os.path.expanduser(os.path.join(os.path.dirname(settings_file), external_split_file)));
        

    #cleanup settings
    cleanup_enabled = settings_values['Cleanup']['cleanup_enabled'].lower() == 'yes';

    ###################################### End of initialization section #############################################
    
    ###################################### Prefiltering section ######################################################
   
    #Gene-gene connections filtering
    gene_connections = gene_connections.loc[(gene_connections['STRING'] >= STRING_threshold) & (gene_connections['BioPlex_Confidence'] >= BioPlex_threshold)];
    printlog('Gene-gene connections after thresholding: %s'%len(gene_connections.index));
    source_indices = list(gene_connections['Gene1']);
    target_indices = list(gene_connections['Gene2']);
   
    #Drug-gene connections filtering 
    printlog('Filtering drug-gene connections...')
    connections = list(compounds[compound_genes_column]);
    
    connections = [str(value).split('|') for value in connections];
    connection_counts = [0] * len(connections);
    
    for i in range(len(connections)):
        if i % 100 == 0:
            printlog('%s of %s'%(i, len(connections)));
        
        values = connections[i];
        #Make sure entries with no connections are represented as an empty list
        if values[0] == 'nan':
            values = [];
        else:
            values = [value.split(':') for value in values];
            
            #Sort connections according to their confidence score highest to lowest
            values = sorted(values, key = lambda item: float(item[1]), reverse = True);
            
            #Remove connections with scores less than gene_compound_threshold
            values = [value for value in values if float(value[1]) >= gene_compound_threshold];
            
            #Make sure that no more than top_genes are left in the final list of connections
            subvalues = values[:top_genes];
            
            #If the final list contains at least one value, use the smallest value as the threshold
            #for selecting connections. This is to ensure that connections with equal score are guaranteed
            #to be equaly selected into the final list. It also means that the final list may have more
            #connections than top_genes if there are more than one connection with the score equal to the last gene score 
            if len(subvalues) > 0:
            
                minscore = np.min(np.array([float(value[1]) for value in subvalues]));
            
                values = [value[0] for value in values if float(value[1]) >= minscore];
            
            
    
        connection_counts[i] = len(values);
        connections[i] = '|'.join(values);
    
    #Calculate resultant connection counts
    compounds[compound_genes_column] = connections;
    compounds['Gene_connection_counts'] = connection_counts;
    
    #Exclude compounds with no connections
    compounds = compounds.loc[compounds[compound_genes_column] != ''];
    printlog('%s compounds with non-zero connections after filtering.'%len(compounds.index));
    
    #Exclude compounds with not enough connections
    compounds = compounds.loc[compounds['Gene_connection_counts'] >= compound_connections];
    
    #Split compounds into classes
    positive_class = compounds.loc[compounds[label_column] == 1];
    printlog('Positive class compound count: %s'%len(positive_class.index));
    
    negative_class = compounds.loc[compounds[label_column] == -1];
    printlog('Negative class compound count: %s'%len(negative_class.index));
    
    other_class = compounds.loc[(compounds[label_column] != -1) & (compounds[label_column] != 1)];
    printlog('Other class compound count: %s'%len(other_class.index));
    
    #Exclude disease entries with no connections
    disease = disease.loc[disease[disease_genes_column] != ''];
    printlog('Disease gene sets: %s'%len(disease.index));
    
    ###################################### End of Prefiltering section ###############################################
   
    
    ###################################### Propagation section #######################################################
    
    if disease_propagation == 'perform':
        #Calculate profile propagation for the disease
        printlog('Working on disease profile(s)...');
        disease_vectors = perform_propagation(list(disease[disease_genes_column]), 
                                              disease, 
                                              'disease', 
                                              genes, 
                                              source_indices, 
                                              target_indices, 
                                              disease_PageRank_c);
        
        if len(disease_vectors.shape) == 1:
            disease_vectors = disease_vectors.reshape(1, -1);

    elif disease_propagation == 'load':
        #Load precalculated disease profile
        disease_vectors = np.fromfile(os.path.join(scratch_path, 'disease_propagated_vectors.dat'), dtype = np.float32).reshape(-1, len(genes.index)).astype(np.float64);
        disease = pd.read_csv(os.path.join(scratch_path, 'disease.csv'), index_col = 0, encoding = 'utf-8');

    else:
        printlog('Propagated disease profiles not available nor calculated. Other steps based on them will be skipped. Reconfigure your settings*.ini if you want different functionality.');
        disease_vectors = None;
        disease = None;
        
        
        
    if (compounds_propagation == 'perform') and not (os.path.isfile(os.path.join(scratch_path, 'positive_class_propagated_vectors.dat'))):    
        #Calculate compound propagated profiles
        printlog('Working on compounds...');
        #Make sure positive, negative and other compounds are propagated equally and as one matrix (n_samples, n_features)
        vectors = perform_propagation(list(positive_class[compound_genes_column]) + list(negative_class[compound_genes_column]) + list(other_class[compound_genes_column]), 
                                              pd.concat([positive_class, negative_class, other_class], ignore_index = True),
                                              'all_classes', 
                                              genes, 
                                              source_indices, 
                                              target_indices, 
                                              compound_PageRank_c);
        
        #Split back into positive, negative and other classes
        positive_vectors = vectors[0 : len(positive_class.index), :];

        negative_vectors = vectors[len(positive_class.index) : (len(positive_class.index) + len(negative_class.index)), :];

        other_vectors = vectors[(len(positive_class.index) + len(negative_class.index)) :, :];
        
        #Save intermediate propagated profiles for individual classes 
        positive_class.to_csv(os.path.join(scratch_path, 'positive_class.csv'), encoding = 'utf-8');
        positive_vectors.astype(np.float32).tofile(os.path.join(scratch_path, 'positive_class_propagated_vectors.dat'));

        negative_class.to_csv(os.path.join(scratch_path, 'negative_class.csv'), encoding = 'utf-8');
        negative_vectors.astype(np.float32).tofile(os.path.join(scratch_path, 'negative_class_propagated_vectors.dat'));

        other_class.to_csv(os.path.join(scratch_path, 'other_class.csv'), encoding = 'utf-8');
        other_vectors.astype(np.float32).tofile(os.path.join(scratch_path, 'other_class_propagated_vectors.dat'));
    
    elif compounds_propagation == 'load':
        #Load previously propagated profiles
        positive_vectors = np.fromfile(os.path.join(scratch_path, 'positive_class_propagated_vectors.dat'), dtype = np.float32).reshape(-1, len(genes.index)).astype(np.float64);
        positive_class = pd.read_csv(os.path.join(scratch_path, 'positive_class.csv'), index_col = 0, encoding = 'utf-8');

        negative_vectors = np.fromfile(os.path.join(scratch_path, 'negative_class_propagated_vectors.dat'), dtype = np.float32).reshape(-1, len(genes.index)).astype(np.float64);
        negative_class = pd.read_csv(os.path.join(scratch_path, 'negative_class.csv'), index_col = 0, encoding = 'utf-8');

        other_vectors = np.fromfile(os.path.join(scratch_path, 'other_class_propagated_vectors.dat'), dtype = np.float32).reshape(-1, len(genes.index)).astype(np.float64);
        other_class = pd.read_csv(os.path.join(scratch_path, 'other_class.csv'), index_col = 0, encoding = 'utf-8');
        
    else:
        printlog('Propagated compound profiles not available nor calculated. Other steps based on them will be skipped. Reconfigure your settings*.ini if you want different functionality.');
        positive_vectors = None;
        positive_class = None;

        negative_vectors = None;
        negative_class = None;

        other_vectors = None;
        other_class = None;
        
        
    ###################################### End of Propagation section ################################################

    ###################################### Correlation section #######################################################

    
    if (positive_vectors is not None) and (negative_vectors is not None) and (other_vectors is not None) and (disease_vectors is not None):
    
        printlog('Performing correlations')
        
        printlog('Working on positive class compounds...');
        #Calculate Pearson correlation between disease and compounds for positive, negative and other classes
        positive_correlations = correlate_all(disease_vectors, positive_vectors)[0];
    
        positive_correlations = positive_correlations.flatten();
    
        printlog('Working on negative class compounds...');
        negative_correlations = correlate_all(disease_vectors, negative_vectors)[0];
    
        negative_correlations = negative_correlations.flatten();
    
        printlog('Working on other class compounds...');
        other_correlations = correlate_all(disease_vectors, other_vectors)[0];
        
        other_correlations = other_correlations.flatten();
    
        #Normalize correlations
    
        all_correlations = np.hstack([positive_correlations, negative_correlations, other_correlations]);
        mean_value = np.mean(all_correlations);
        std_value = np.std(all_correlations);
    
        positive_correlations = (positive_correlations - mean_value) / std_value;
        negative_correlations = (negative_correlations - mean_value) / std_value;
        other_correlations = (other_correlations - mean_value) / std_value;
        
        positive_class['Normalized_correlation'] = positive_correlations.tolist();
        negative_class['Normalized_correlation'] = negative_correlations.tolist();
        other_class['Normalized_correlation'] = other_correlations.tolist();
        
        #Estimate r-values relative to the negative class
        positive_rvals = [];
        for i in range(len(positive_correlations)):
            positive_rvals.append(np.sum(negative_correlations > positive_correlations[i]) / len(negative_correlations));
        
        other_rvals = [];
        for i in range(len(other_correlations)):
            other_rvals.append(np.sum(negative_correlations > other_correlations[i]) / len(negative_correlations));
        
        negative_rvals = np.zeros((len(negative_correlations), ), dtype = np.float64);
        
        reindex = np.argsort(-negative_correlations);
    
        negative_rvals[reindex] = np.arange(len(negative_correlations), dtype = np.float64) / len(negative_correlations);
        
        positive_class['r_vals(neg)'] = positive_rvals;
        negative_class['r_vals(neg)'] = negative_rvals.tolist();
        other_class['r_vals(neg)'] = other_rvals;
    
    else:
        printlog('Missing correlations due to abscent propagated profiles - cannot perform other steps such as prediction and analysis. Please change your settings*.ini accordingly.')
        do_predictions = False;
        do_analysis = False;
    
    if do_prediction:
        printlog('Predicting probability')
        clf = LogisticRegression(fit_intercept = True, class_weight = 'balanced').fit(np.hstack([positive_correlations, negative_correlations]).reshape(-1, 1), 
                                                                                      np.hstack([np.ones((len(positive_correlations), ), dtype = np.int32),
                                                                                                  np.zeros((len(negative_correlations), ), dtype = np.int32)
                                                                                                  ]));
        
        
        positive_prob = clf.predict_proba(positive_correlations.reshape(-1, 1))[:, clf.classes_ == 1].flatten();
        negative_prob = clf.predict_proba(negative_correlations.reshape(-1, 1))[:, clf.classes_ == 1].flatten();
        other_prob = clf.predict_proba(other_correlations.reshape(-1, 1))[:, clf.classes_ == 1].flatten();
        
        
        
        positive_class['Probability_estimate'] = positive_prob.tolist();
        negative_class['Probability_estimate'] = negative_prob.tolist();
        other_class['Probability_estimate'] = other_prob.tolist();
   
    printlog('Saving compounds with predictions')
    total_compounds = pd.concat([positive_class, negative_class, other_class]);
   
    total_compounds.to_csv(os.path.join(results_path, 'Compounds.csv'));
    
    if export_multiplied or export_original:
        printlog('Exporting RNK files for GSEA analysis');
        
        gene_names = list(genes['Name']);
        gene_names = [str(value).split('|') for value in gene_names];

        if (disease_vectors is not None) and (len(disease_vectors) > 1)  and (export_original) and (export_disease):
            for i in range(len(disease_vectors)):
                vector = disease_vectors[i, :];
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;
                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'disease_%s.rnk'%i));
        elif (disease_vectors is not None)  and (export_original) and (export_disease):
            vector = disease_vectors[0, :];
            median = np.median(vector[vector > 0.0]);
            vector = vector / median;

            export_vector_rnk(vector, gene_names, os.path.join(results_path, 'disease.rnk'));


        if (positive_vectors is not None) and (export_original) and (export_positive):
            printlog('Exporting positive compound RNKs')        
            positive_ids = list(positive_class['ID']);
            for i in range(len(positive_ids)):
                print(i);
                vector = positive_vectors[i, :];
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;

                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'positive_%s_%s.rnk'%(positive_ids[i].replace('|', '_'), positive_correlations[i])));

        if (negative_vectors is not None) and (export_original) and (export_negative):
            printlog('Exporting negative compound RNKs')        
            negative_ids = list(negative_class['ID']);
            for i in range(len(negative_ids)):
                print(i);
                vector = negative_vectors[i, :];
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;

                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'negative_%s_%s.rnk'%(negative_ids[i].replace('|', '_'), negative_correlations[i])));
        
        if (other_vectors is not None) and (export_original) and (export_other):
            printlog('Exporting other compound RNKs')        
            other_ids = list(other_class['ID']);
            for i in range(len(other_ids)):
                print(i);
                vector = other_vectors[i, :];
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;

                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'other_%s_%s.rnk'%(other_ids[i].replace('|', '_'), other_correlations[i])));



        if (positive_vectors is not None) and (disease_vectors is not None) and (export_multiplied) and (export_positive):
            printlog('Exporting positive compound RNKs multiplied by disease')        
            positive_ids = list(positive_class['ID']);
            for i in range(len(positive_ids)):
                print(i);
                vector = positive_vectors[i, :];
                
                vector = np.multiply(disease_vectors, vector.reshape(1, -1));
                if vector.shape[0] != 1:
                    vector = np.median(vector, axis = 0) / np.std(vector, axis = 0);
                
                vector = vector.flatten();
                
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;
                
                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'mul_positive_%s_%s.rnk'%(positive_ids[i].replace('|', '_'), positive_correlations[i])));

        if (negative_vectors is not None) and (disease_vectors is not None) and (export_multiplied) and (export_negative):
            printlog('Exporting negative compound RNKs multiplied by disease')        
            negative_ids = list(negative_class['ID']);
            for i in range(len(negative_ids)):
                print(i);
                vector = negative_vectors[i, :];

                vector = np.multiply(disease_vectors, vector.reshape(1, -1));
                if vector.shape[0] != 1:
                    vector = np.median(vector, axis = 0) / np.std(vector, axis = 0);
                
                vector = vector.flatten();
                
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;
                
                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'mul_negative_%s_%s.rnk'%(negative_ids[i].replace('|', '_'), negative_correlations[i])));
        
        if (other_vectors is not None) and (disease_vectors is not None) and (export_multiplied) and (export_other):
            printlog('Exporting other compound RNKs multiplied by disease')        
            other_ids = list(other_class['ID']);

            
            
            for i in range(len(other_ids)):
                print(i);
                vector = other_vectors[i, :];

                vector = np.multiply(disease_vectors, vector.reshape(1, -1));
                if vector.shape[0] != 1:
                    vector = np.median(vector, axis = 0) / np.std(vector, axis = 0);
                
                vector = vector.flatten();
            
                median = np.median(vector[vector > 0.0]);
                vector = vector / median;

                export_vector_rnk(vector, gene_names, os.path.join(results_path, 'mul_other_%s_%s.rnk'%(other_ids[i].replace('|', '_'), other_correlations[i])));

    
    ###################################### End of Correlation section ################################################

    ###################################### Analysis section ##########################################################
    if do_analysis:
        printlog('Analysing selected parameters.');
        
        X = np.hstack([positive_correlations, negative_correlations]).reshape(-1, 1);
        y = np.hstack([np.ones((len(positive_correlations), ), dtype = np.int32), np.zeros((len(negative_correlations), ), dtype = np.int32)]);
        
        printlog('X: %s'%str(X.shape));
        printlog('y: %s'%str(y.shape));
        printlog('N positive: %s'%len(positive_correlations));
        printlog('N negative: %s'%len(negative_correlations));

        #One can use external splits file instead of generating one. This can be used to ensure splits are the same between different runs, e.g. for global cross-validation
        if os.path.isfile(external_split_file):
            experimental_splits = pd.read_csv(external_split_file, index_col = 0, encoding = 'utf-8');
            
        else:
            train_splits = [];
            test_splits = [];

            for i in range(n_repeats):
                splitter = StratifiedKFold(n_splits = cv_folds, shuffle = True);
                for train_index, test_index in splitter.split(X, y):
                    train_splits.append('|'.join([str(value) for value in train_index]));
                    test_splits.append('|'.join([str(value) for value in test_index]));
            
            experimental_splits = pd.DataFrame({'Train_splits' : train_splits, 'Test_splits' : test_splits});
            
            experimental_splits.to_csv(os.path.join(results_path, 'train_test_splits.csv'), encoding = 'utf-8');
        
        #Aggregate and average results from multiple splits
        pos_y_train = [];
        pos_y_test = [];
        y_train = [];
        y_test = [];
        baccuracy_train = [];
        baccuracy_test = [];
        sensitivity_train = [];
        sensitivity_test = [];
        specificity_train = [];
        specificity_test = [];
        

        for index in experimental_splits.index:
            print('Split %s of %s'%(index, len(experimental_splits.index)));
            
            train_index = experimental_splits['Train_splits'][index].split('|');
            train_index = np.array([int(value) for value in train_index]);
            
            test_index = experimental_splits['Test_splits'][index].split('|');
            test_index = np.array([int(value) for value in test_index]);
            
            result = get_accuracies(X, y, train_index, test_index);
        
            pos_y_train.append(result[0]);
            pos_y_test.append(result[1]);
            y_train.append(result[2]);
            y_test.append(result[3]);
            baccuracy_train.append(result[4]);
            baccuracy_test.append(result[5]);
            sensitivity_train.append(result[6]);
            sensitivity_test.append(result[7]);
            specificity_train.append(result[8]);
            specificity_test.append(result[9]);
        
        
        classification_stats = pd.DataFrame({
            'SplitIndex' : list(experimental_splits.index),
            'Settings_file' : [os.path.basename(settings_file)] * len(experimental_splits.index),
            'Disease' : [os.path.basename(disease_file)] * len(experimental_splits.index),
            'Disease_genes_column' : [disease_genes_column] * len(experimental_splits.index),
            'Compounds' : [os.path.basename(compounds_file)] * len(experimental_splits.index),
            'Compound_genes_column' : [compound_genes_column] * len(experimental_splits.index),
            'Compound_gene_threshold' : [gene_compound_threshold] * len(experimental_splits.index),
            'Label_column' : [label_column] * len(experimental_splits.index),
            'STRING_threshold' : [STRING_threshold] * len(experimental_splits.index),
            'BioPlex_threshold' : [BioPlex_threshold] * len(experimental_splits.index),
            'Disease_PageRank_c' : [disease_PageRank_c] * len(experimental_splits.index),
            'Compound_PageRank_c' : [compound_PageRank_c] * len(experimental_splits.index),
            'pos_y_train' : pos_y_train,
            'pos_y_test' : pos_y_test,
            'y_train' : y_train,
            'y_test' : y_test,
            'baccuracy_train' : baccuracy_train,
            'baccuracy_test' : baccuracy_test,
            'sensitivity_train' : sensitivity_train,
            'sensitivity_test' : sensitivity_test,
            'specificity_train' : specificity_train,
            'specificity_test' : specificity_test,
            
            });
        classification_stats.to_csv(os.path.join(results_path, 'classification_stats.csv'));
        
        
        mean_classification_stats = pd.DataFrame({
            'Settings_file' : [os.path.basename(settings_file)],
            'Disease' : [os.path.basename(disease_file)],
            'Disease_genes_column' : [disease_genes_column],
            'Compounds' : [os.path.basename(compounds_file)],
            'Compound_genes_column' : [compound_genes_column],
            'Compound_gene_threshold' : [gene_compound_threshold],
            'Label_column' : [label_column],
            'STRING_threshold' : [STRING_threshold],
            'BioPlex_threshold' : [BioPlex_threshold],
            'Disease_PageRank_c' : [disease_PageRank_c],
            'Compound_PageRank_c' : [compound_PageRank_c],
            'mean_pos_y_train' : np.mean(np.array(pos_y_train, dtype = np.float64)),
            'mean_pos_y_test' : np.mean(np.array(pos_y_test, dtype = np.float64)),
            'mean_y_train' : np.mean(np.array(y_train, dtype = np.float64)),
            'mean_y_test' : np.mean(np.array(y_test, dtype = np.float64)),
            'mean_baccuracy_train' : np.mean(np.array(baccuracy_train, dtype = np.float64)),
            'mean_baccuracy_test' : np.mean(np.array(baccuracy_test, dtype = np.float64)),
            'mean_sensitivity_train' : np.mean(np.array(sensitivity_train, dtype = np.float64)),
            'mean_sensitivity_test' : np.mean(np.array(sensitivity_test, dtype = np.float64)),
            'mean_specificity_train' : np.mean(np.array(specificity_train, dtype = np.float64)),
            'mean_specificity_test' : np.mean(np.array(specificity_test, dtype = np.float64)),

            'std_pos_y_train' : np.std(np.array(pos_y_train, dtype = np.float64)),
            'std_pos_y_test' : np.std(np.array(pos_y_test, dtype = np.float64)),
            'std_y_train' : np.std(np.array(y_train, dtype = np.float64)),
            'std_y_test' : np.std(np.array(y_test, dtype = np.float64)),
            'std_baccuracy_train' : np.std(np.array(baccuracy_train, dtype = np.float64)),
            'std_baccuracy_test' : np.std(np.array(baccuracy_test, dtype = np.float64)),
            'std_sensitivity_train' : np.std(np.array(sensitivity_train, dtype = np.float64)),
            'std_sensitivity_test' : np.std(np.array(sensitivity_test, dtype = np.float64)),
            'std_specificity_train' : np.std(np.array(specificity_train, dtype = np.float64)),
            'std_specificity_test' : np.std(np.array(specificity_test, dtype = np.float64)),
            
            });
        mean_classification_stats.to_csv(os.path.join(results_path, 'mean_classification_stats.csv'));
        
    


    ###################################### End of Analysis section ###################################################

    
    if cleanup_enabled:
        printlog('Removing intermediate files...')
        # shutil.rmtree(scratch_path);
        
    
    
    #Finalization stats and timing
    printlog('\nFinished on %s in'%(time.strftime("%a, %d %b %Y at %H:%M:%S")));   
    toc();
    
    memory_report();
    
    #Print description ending here
    printlog(settings.description_epilog);
    
    #stop logging
    stop_log();

